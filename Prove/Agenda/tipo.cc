#include <iostream>
#include <cstring>
#include <fstream>
using namespace std;

#include "tipo.h"

int compare(tipo_inf a,tipo_inf b){
    return strcmp(a.data,b.data);
}

void copy(tipo_inf& a,tipo_inf b){
    strcpy(a.data,b.data);
    strcpy(a.inizio,b.inizio);
    strcpy(a.fine,b.fine);
    strcpy(a.descr,b.descr);
}

void print(tipo_inf a){
    cout<<a.data<<" "<<a.inizio<<" "<<a.fine<<" "<<a.descr<<endl;
}