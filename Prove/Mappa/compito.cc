#include <iostream>
#include <cstring>
#include <fstream>
#include <stdlib.h>
using namespace std;

#include "tipo.h"
#include "liste.h"
#include "grafo.h"

graph mappa(int n){
    graph g=new_graph(n);
    int v1,v2;
    ifstream f("G.txt");

    while (f.good()){
        f>>v1>>v2;
        add_edge(g,v1,v2,0);
    }
    return g;
}

void stampa(lista pi){
    while (pi!=NULL){
        cout<<pi->inf.nome;
        if (tail(pi)==NULL){
            break;}
        else
            cout<<", ";
        pi=tail(pi);
    }
}

lista genera(lista pi, char tipo){
    lista nuova=NULL;

    while (pi!=NULL){
        if (pi->inf.tipo==tipo){
            elem* a=new_elem(pi->inf);
            nuova=ord_insert_elem(nuova,a);
        }
            pi=tail(pi);
    }
    return nuova;
}

tipo_inf search_pi(lista pi, int id){
    tipo_inf search;
    search.id=id;

    while (pi!=NULL && compare(pi->inf,search)<=0){ 
        if (compare(pi->inf,search)==0)
            return pi->inf;
        else
            pi=tail(pi);               
    }
    search.id=0;
    return search;
}

void stampa_mappa(graph g, lista pi){ //non va?
    adj_list tmp;
    for (int i=1; i<=get_dim(g); i++) {
        tmp=get_adjlist(g,i);

        tipo_inf search=search_pi(pi,i);
        cout<<search.nome<<" connesso a: ";

        while (tmp) {
            tipo_inf adj=search_pi(pi,get_adjnode(tmp));
            cout<<adj.nome<<", ";
            tmp=get_nextadj(tmp);
        }
    cout << endl;
    }
}

int carica(lista& pi){
    int n=0;
    tipo_inf posti;

    ifstream f ("PI.txt");
    while (f.good()){
        f>>posti.id;
        f.ignore();
        f.getline(posti.nome,21);
        f>>posti.tipo;
        
        if (f.good()==false)
            break;

        n++;
        elem* a=new_elem(posti);
        pi=ord_insert_elem(pi,a);
    }

    f.close();
    return n;
}

int main(){

    lista pi=NULL;
    tipo_inf search;
    int n=carica(pi);

    int id;
    for(;;){
        cout<<"Inserisci id da cercare oppure 0 per terminare: ";
        cin>>id;
        if (id==0)
            break;
        else{
            search=search_pi(pi,id);
            if (search.id==0)
                cout<<"Non esiste"<<endl;
            else{
                print(search); cout<<endl;}
        }      
    }

    //punto 2.b
    char tipo;
    cout<<"Inserisci tipo: "; cin>>tipo;
    lista nuova=NULL;
    nuova=genera(pi,tipo);
    stampa(nuova); cout<<endl;

    //punto 2.a
    graph g=mappa(n);
    stampa_mappa(g,pi);
}