#include <iostream>
#include <cstring>
#include <fstream>
using namespace std;

#include "ospedale.h"
#include "grafo.h"

void stampa(graph G, tipo_inf* h){
	adj_list temp;

	for(int i = 1; i <= get_dim(G); i++){
		temp = get_adjlist(G,i);
	    while(temp){

	    	if(h[i].cittadino_medico == 'M' && h[temp->node+1].cittadino_medico == 'C')
                cout<<"MEDICO "<<i<<" CURA PAZIENTE "<<get_adjnode(temp)<<endl;

	    	if(h[i].cittadino_medico == 'C' && h[temp->node+1].cittadino_medico == 'C')
	    		cout<<"PAZIENTE "<<i<<" IN CAMERA CON PAZIENTE "<<get_adjnode(temp)<<endl;

	        temp = get_nextadj(temp);
	    }
	}
}


int main(){

    int n_nodi,n_archi,v1,v2,id;
    ifstream f("ospedale");
    f>>n_nodi;
    f>>n_archi;
    graph g=new_graph(n_nodi);
    tipo_inf* osp=new tipo_inf[n_nodi];


    for (int i=1;i<=n_nodi;i++){
        f>>id;
        f>>osp[i].cittadino_medico;
        f>>osp[i].positivo_negativo;
    }
    for (int i=0;i<n_archi;i++){
        f>>v1;
        f>>v2;
        add_edge(g,v1,v2,0);
    }

    f.close();
    stampa(g,osp);
}
