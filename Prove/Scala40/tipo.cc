#include <iostream>
#include <cstring>
using namespace std;

#include "tipo.h"

int compare(tipo_inf a,tipo_inf b){
    if((a.seme == b.seme) && ((a.valore < b.valore) || (a.valore>b.valore)))
		return a.valore - b.valore;
	else
		return a.seme - b.seme;  
}

void copy(tipo_inf& a,tipo_inf b){
    a=b;
}

void print(tipo_inf a){
    cout<<a.valore<<a.seme;
}