#include <iostream>
#include <cstring>
#include <fstream>
using namespace std;

#include "op.h"
#include "bst.h"

void stampa(bst b, char data[], char tipo){
    if (b==NULL)
        return;

    if (strcmp(b->key,data)==0 && b->inf.tipo==tipo){
        if (b->inf.deb_cred==true) //true=debito
            cout<<"-"<<b->inf.cifra;
        else
            cout<<"+"<<b->inf.cifra;
    }

    else{
        if (strcmp(data,b->key)<0){
            if (b->left!=NULL)
                stampa(b->left,data,tipo);
            else{
                cout<<"Operazione non trovata.";
                return;
            }
        }
        else{
            if (b->right!=NULL)
                stampa(b->right,data,tipo);
            else{
                cout<<"Operazione non trovata.";
                return;
            }
        }
    }
}

bst estrai(bst b, char data[]){
    bst nuovo=NULL;
    
    while (b!=NULL){
        if (strcmp(b->key,data)==0 || strcmp(b->key,data)>0){
            bnode* a=bst_newNode(b->key,b->inf);
            bst_insert(nuovo,a);
        } 
        if (b->right!=NULL)
            b=b->right;
        else{
            if (b->left!=NULL)
                b=b->left;
            else
                break;
        }
    }
    return nuovo;
}

float saldo(bst b){
    if (b==NULL)
        return 0;
    if (b->inf.deb_cred==false) // credito = sommo
        return saldo(b->left) + saldo(b->right)+b->inf.cifra;
    else
        return saldo(b->left) + saldo(b->right)-b->inf.cifra;
}

int main(){

    bst btree=NULL;
    tipo_inf conto;

    ifstream f("conto.txt");
    int n; f>>n;
    char data[7];

    for (int i=0;i<n;i++){
        f>>data;
        f>>conto.cifra;
        f>>conto.deb_cred;
        f>>conto.tipo;

        bnode* a=bst_newNode(data,conto);
        bst_insert(btree,a);
    }

    f.close();
    print_BST(btree);

    float s;
    s=saldo(btree);
    cout<<s<<endl;
    
    cout<<"Inserisci data: ";
    cin>>data;
    bst nuovo=estrai(btree,data);
    print_BST(nuovo);

    cout<<"Inserisci data da cercare: ";
    cin>>data;
    cout<<"Inserisci tipo: ";
    char tipo;cin>>tipo;
    stampa(btree,data,tipo);

}