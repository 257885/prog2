#include <iostream>
#include <cstring>
#include <fstream>
using namespace std;

#include "node.h"
#include "grafi.h"
#include "coda.h"

void stampa(graph g){
    adj_list tmp;
    for (int i=1; i<=get_dim(g); i++) {
        tmp=get_adjlist(g,i);
        cout << i;
        while (tmp) {
            cout << " --> " << get_adjnode(tmp);
            tmp=get_nextadj(tmp);
        }
    cout << endl;
    }
}

void stampaRelazioni(graph G, node* tweetter){
	adj_list temp;
	for(int i = 1; i <= get_dim(G); i++){
		temp = get_adjlist(G,i);
	    while(temp){

	    	cout<<tweetter[i-1].cont;

	    	if(tweetter[i-1].tipo == 'U' && tweetter[temp->node].tipo == 'U')
	    		cout<<" FOLLOW "<<tweetter[temp->node].cont<<endl;
	    	if(tweetter[i-1].tipo == 'T' && tweetter[temp->node].tipo == 'U')
	    		cout<<" OWNER "<<tweetter[temp->node].cont<<endl;
	    	if(tweetter[i-1].tipo == 'U' && tweetter[temp->node].tipo == 'T')
	    		cout<<" LIKE "<<tweetter[temp->node].cont<<endl;
	        temp = get_nextadj(temp);
	    }
	}
}

int main(){
    int n,v1,v2;
    
    ifstream f("graph");
    f>>n;
    graph g=new_graph(n);

    /*while (f.good()){
        if (f.good()==false)
            break;
        f>>v1;
        f>>v2;
        add_arc(g,v1,v2,0);
    }*/

    while (f>>v1>>v2){
        add_arc(g,v1,v2,0);
    }

    f.close();
    stampa(g);

    node* a = new node[get_dim(g)];
    ifstream f2("node");

	for(int i = 0; i < get_dim(g); i++){
		f2.getline(a[i].cont, 81);
		f2>>a[i].tipo;

		f2.ignore();
	}
    f2.close();
    stampaRelazioni(g, a);
	cout<<endl;


}
