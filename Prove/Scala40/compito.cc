#include <iostream>
#include <cstring>
using namespace std;

#include "tipo.h"
#include "liste.h"

void pesca(lista& l){
    tipo_inf a;
    cout<<"Inserisci valore della carta: ";
    cin>>a.valore;

    cout<<"Inserisci seme della carta: ";
    cin>>a.seme;

    elem* b=new_elem(a);
    l=ord_insert_elem(l,b);
}

void stampa(lista l){
    while (l!=NULL){
        print(l->inf);
        cout<<" ";
        l=l->pun;
    }
}

int main(){
    lista gioc1=NULL;
    lista gioc2=NULL;
    int n;

    cout<<"Quante carte assegnare ai due giocatori?"<<endl;
    cin>>n;

    for (int i=0;i<n;i++){
        cout<<"GIOCATORE 1: inserire carta numero "<<i+1<<endl;
        pesca(gioc1);
    }

    for (int i=0;i<n;i++){
        cout<<"GIOCATORE 2: inserire carta numero "<<i+1<<endl;
        pesca(gioc2);
    }

    cout<<"Giocatore 1: ";
    stampa(gioc1);
    cout<<endl;

    cout<<"Giocatore 2: ";
    stampa(gioc2);
}