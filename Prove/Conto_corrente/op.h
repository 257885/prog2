typedef struct{
	float cifra;
	bool deb_cred; /** indica se l’operazione è a debito o a credito (true = debito, false = credito) */
	char tipo; /** tipo dell'operazione effettuata (B – Bonifico, U – utenza, C – carta)  */
}tipo_inf;

int compare(tipo_inf, tipo_inf);
void copy(tipo_inf&, tipo_inf);
void print(tipo_inf);