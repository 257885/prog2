OBJ= compito.o 
CXXFLAGS=-Wall

Esame: $(OBJ)
	g++ -o Esame $(OBJ)

-include dependencies

.PHONY: depend clean cleanall
depend:
	g++ -MM *.cc > dependencies
clean:
	rm -f *.o
cleanall:
	rm -f Esame *.o
