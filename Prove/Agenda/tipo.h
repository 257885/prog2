/*
 * tipo.h
 *
 *  Created on: 17 set 2021
 *      Author: Andra
 */

typedef struct {
	char data[7];
	char inizio[5];
	char fine[5];
	char descr[51];
} tipo_inf;

int compare(tipo_inf,tipo_inf);
void copy(tipo_inf&,tipo_inf);
void print(tipo_inf);