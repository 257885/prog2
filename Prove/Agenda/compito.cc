/*
 * compito.cc
 *
 *  Created on: 17 set 2021
 *      Author: Alexandra
 */

#include <iostream>
#include <cstring>
#include <fstream>
using namespace std;

#include "tipo.h"
#include "liste.h"

/**
 * Funzione che stampa un agenda.
 *
 * Stampa il contenuto di un agenda scorrendo tutta la lista
 *
 * @param l Lista dell'agenda
 */
void stampa(lista l){
    while (l!=NULL){
        print(l->inf);
        l=tail(l);
    }
}

/**
 * Funzione agenda che carica in una lista un nuovo appuntamento
 *
 * Carica nella lista passata e ne modifica il contentuto aggiungendo
 * il nuovo appuntamento,
 * controllando prima se per quella data e ora è già occupata.
 *
 * @param l Lista dell'agenda
 * @param t un appuntamento
 */
void agenda(lista& agenda,tipo_inf a){
    bool f=true;
    lista l1=agenda;

    while (l1!=NULL){
        if (strcmp(l1->inf.data,a.data)==0 && (strcmp(l1->inf.inizio,a.inizio)==0)){
            f=false;
            break;
        }
        else{
            f=true;
            l1=tail(l1);
        }
    }
    if (f){
        elem* n=new_elem(a);
        agenda=insert_elem(agenda,n);
    }
    else{
        cout<<"Errore per: ";
        print(a); //return
    }
}

/**
 * Funzione per stampare il numero di appuntamenti basato SULL'ORARIO DI INIZIO
 * 
 * @param agenda Lista dell'agenda
 * @param a Un appuntamento
 */
void quanti(lista agenda, tipo_inf a){
    int precedenti,seguenti=0;

    if (agenda==NULL)
        return;
    
    while (agenda!=NULL){
        if (strcmp(agenda->inf.data,a.data)==0){
            if (strcmp(agenda->inf.inizio,a.inizio)<0) //a.fine
                precedenti++;
            else if (strcmp(agenda->inf.inizio,a.inizio)>0) //a.fine 
                    seguenti++;
        }
        agenda=tail(agenda);
    }
    cout << precedenti << " appuntamenti prima"<<endl;
	cout << seguenti << " appuntamenti dopo"<<endl;
}

/**
 * Main del programma per l'acquisizione e stampa dati
 *
 */
int main(){

    lista ag=NULL;
    tipo_inf app;

    ifstream f("agenda.txt");
    while (f.good()){
    f>>app.data;
    f>>app.inizio;
    f>>app.fine;
    f.ignore();
    f.getline(app.descr,50);
    agenda(ag, app);
    }

    f.close();
    cout<<"--------Agenda finale----------"<<endl;
    stampa(ag);
    cout<<endl;

    cout << "Inserisci data,inizio e fine separati da uno spazio: ";
	cin >> app.data >> app.inizio >>app.fine;
	cout << "Inserisci breve descrizione: ";
	cin >> app.descr;
    quanti(ag,app);
}

