#include <iostream>
#include <cstring>
using namespace std;
#include "carta.h"
#include "bst.h"

void aggiorna(bst &b, int cod, int punti){
	bnode* search = bst_search(b,cod);
	if(search == NULL)
		cout<<"Carta "<<cod<<" non esistente!"<<endl;

	else
		search->inf.totpunti += punti;
}

int totalePunti(bst b, int inf, int sup){
    if (b==NULL)    
        return 0;
    
    if (b->key >= inf && b->key <= sup)
        return b->inf.totpunti + totalePunti(b->left,inf,sup) + totalePunti(b->right,inf,sup);
    else{
        if (b->key >= inf)
            return totalePunti(b->left,inf,sup);
        else 
            return totalePunti(b->right,inf,sup);
    }
}

int main(){
    bst tree=NULL;
    tipo_inf carta;
    int n;
    int codice;
    cout<<"Inserisci numero di carte: ";
    cin>>n;

    for (int i=0;i<n;i++){
        cout<<"Inserisci codice: ";
        cin>>codice;

        cout<<"Inserisci nome e cognome: ";
        cin.ignore();
        cin.getline(carta.nome,41);

        cout<<"Inserisci totale dei punti: ";
        cin>>carta.totpunti;

        bnode* a=bst_newNode(codice,carta);
        bst_insert(tree,a);
    }
    cout<<endl;
    stampa(tree);
    cout<<endl;

    cout<<"Inserisci limite inferiore: ";
    int inf; cin>>inf;
    cout<<"Inserisci limite superiore: ";
    int sup; cin>>sup;

    int somma=totalePunti(tree,inf,sup);
    cout<<somma<<endl;

    int cod,punti;
    for(;;){
        cout<<"Inserisci codice di carta oppure 0 per terminare: ";
        cin>>cod;
        if (cod==0)
            break;
        cout<<"Inserisci punti: ";
        cin>>punti;

        aggiorna(tree,cod,punti);
    }
    
    cout<<endl;
    cout<<"Nuovo BST aggiornato: "<<endl;
    stampa(tree);

}