#include <iostream>
#include <cstring>
#include <fstream>
using namespace std;

#include "tipo.h"

int compare(tipo_inf a,tipo_inf b){
    return a.id-b.id;
}

void copy(tipo_inf& a,tipo_inf b){
    a.id=b.id;
    strcpy(a.nome,b.nome);
    a.tipo=b.tipo;
}

void print(tipo_inf a){
    cout<<a.nome<<" "<<a.tipo;
}