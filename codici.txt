----STAMPA GRAFO---
void stampa(graph g){
    adj_list tmp;
    for (int i=1; i<=get_dim(g); i++) {
        tmp=get_adjlist(g,i);
        cout << i;
        while (tmp) {
            cout << " --> " << get_adjnode(tmp);
            tmp=get_nextadj(tmp);
        }
    cout << endl;
    }
}


----STAMPA ALBERO NORMALE----
void serialize(tree t){
    cout<<"(";
    print(get_info(t));
    tree t1 = get_firstChild(t);
    while(t1!=NULL){
        serialize(t1);
        t1 = get_nextSibling(t1);
    }
    cout<<")";
}


----STAMPA BST----
void print_BST(bst b){
    if(b==NULL)
        return;
    if(get_left(b)!=NULL)
        print_BST(get_left(b));
    print_key(get_key(b));
    cout<<" ";
    print(get_value(b));
    cout<<endl;
    if(get_right(b)!=NULL)
        print_BST(get_right(b));
}

----DIMENSIONE TREE (n° di nodi) DFS----
int dimensione(tree t){
		int dim=0,dim1;
		tree t1 = get_firstChild(t);
		while(t1!=NULL){
			dim1=dimensione(t1);
			dim+=dim1;
			t1 = get_nextSibling(t1);
		}
		return dim+1;
}

----DIMENSIONE TREE (n° di nodi) BFS----
int dimensione(tree t){
	int count=0;
	codaBFS c = newQueue();
	c=enqueue(c,t);
	while(!isEmpty(c)){
		node* n=dequeue(c);
		count++;
		tree t1 = get_firstChild(n);
		while(t1!=NULL){
				c=enqueue(c,t1);
				t1 = get_nextSibling(t1);
			}
	}
	return count;
}

----INSERT BST RICORSIVA----
void bst_insert(bst& b, bnode* n){
    if(b==NULL){
        b=n;
        return;
    }
    if (compare_key(get_key(n),get_key(b))<0){
        if(get_left(b)!=NULL)
            bst_insert(b->left,n);
        else {
            b->left=n;
            n->parent=b;
        }
    }
    else{
        if(get_right(b)!=NULL)
            bst_insert(b->right,n);
        else {
            b->right=n;
            n->parent=b;
        }
    }
}