#include <iostream>
#include <cstring>
using namespace std;

#include "op.h"

int compare(tipo_inf a, tipo_inf b){
    return a.cifra-b.cifra;
}

void copy(tipo_inf& a, tipo_inf b){
    a.cifra=b.cifra;
    a.deb_cred=b.deb_cred;
    a.tipo=b.tipo;
}

void print(tipo_inf a){
    cout<<a.cifra<<" "<<a.deb_cred<<" "<<a.tipo;
}